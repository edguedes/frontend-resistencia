import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { RebelComponent } from './rebel/rebel.component';
import { MaterialModule } from '../material.module';
import { RebelFormComponent } from './rebel/rebel-form/rebel-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RebelDetailComponent } from './rebel/rebel-detail/rebel-detail.component';
import { MessageComponent } from './utils/message/message.component';
import { ReportComponent } from './rebel/report/report.component';

@NgModule({
  declarations: [
    RebelComponent, 
    RebelFormComponent, 
    RebelDetailComponent, 
    ReportComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    CoreRoutingModule,
    FormsModule, 
    ReactiveFormsModule
  ],
  providers: [
    MessageComponent
  ]
})
export class CoreModule { }
