import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RebelComponent } from './rebel/rebel.component';
import { RebelFormComponent } from './rebel/rebel-form/rebel-form.component';
import { RebelDetailComponent } from './rebel/rebel-detail/rebel-detail.component';
import { ReportComponent } from './rebel/report/report.component';

const routes: Routes = [
  { path: '', component: RebelComponent },
  { path: 'new', component: RebelFormComponent },
  { path: 'report', component: ReportComponent},
  { path: ':token', component: RebelDetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
