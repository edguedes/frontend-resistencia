import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Report } from './dto/report';
import { RebelService } from '../rebel.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.sass']
})
export class ReportComponent implements OnInit {

reportForm: FormGroup;
report: Report = null;

  constructor(
    private service: RebelService
  ) { }

  ngOnInit(): void {
    this.getReport();
  }

  getReport(){
    this.service.getReport().subscribe((result) => {
      this.report = result,
      console.log(this.report)
    });
  }

}
