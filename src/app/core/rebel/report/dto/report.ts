export interface Report {
    percentageTraitors?: string;
    percentageRebel?: string;
    mediaRebelItems?: any;
    lostPointsTraitors?: number;
}