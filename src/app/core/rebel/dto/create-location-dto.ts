export class CreateLocationDTO {
    id: number;
    base: string;
    latitude: number;
    longitude: number;

    constructor(){
        this.id = null;
        this.base = "";
        this.latitude = 0;
        this.longitude = 0;
    }
}
