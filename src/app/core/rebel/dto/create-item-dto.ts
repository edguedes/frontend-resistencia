export class CreateItemDTO {
    id: number;
    resource: string;
    valueItem?: number;

    constructor(){
        this.id = null;
        this.resource = "";
        this.valueItem = 0;
    }
}
