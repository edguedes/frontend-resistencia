import { CreateLocationDTO } from './create-location-dto';
import { CreateItemDTO } from './create-item-dto';

export class CreateRebelDTO {
    id?: number;
    name: string;
    token?: string;
    age: number;
    score?: number;
    traitor?: boolean;
    genre: string;
    locationDTO: CreateLocationDTO;
    itensDTO: string[];

    constructor(){
        this.id = null;
        this.name = "";
        this.token = "";
        this.age = 0;
        this.score = 0;
        this.traitor = false;
        this.genre = "";
        this.locationDTO = new CreateLocationDTO();
        this.itensDTO = [];
    }
}
