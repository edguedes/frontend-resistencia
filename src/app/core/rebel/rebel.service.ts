import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RebelList } from './rebel-list';
import { CreateRebelDTO } from './dto/create-rebel-dto';
import { RebelDetail } from './rebel-detail/dto/rebel-detail';

@Injectable({
  providedIn: 'root'
})
export class RebelService {
  
  readonly urlBase = 'http://localhost:8080/api/rebels';

  constructor(private http: HttpClient) { }

  getAllRebel(): Observable<RebelList[]> {
    return this.http.get<RebelList[]>(this.urlBase);
  }

  getRebelToken(token: String): Observable<RebelDetail> {
    return this.http.get<RebelDetail>(`${this.urlBase}/${token}`);
  }

  getReport(): Observable<any> {
    return this.http.get(`${this.urlBase}/reports`);
  }

  createRebel(rebelDto: CreateRebelDTO): Observable<any> {
    return this.http.post(this.urlBase, rebelDto);
  }

  negotiate(rebelsInvolvedDto: any): Observable<any> {
    return this.http.post(`${this.urlBase}/negotiation`, rebelsInvolvedDto);
  }
}
