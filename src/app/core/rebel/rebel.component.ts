import { Component, OnInit } from '@angular/core';
import { RebelService } from './rebel.service';
import { MessageComponent } from '../utils/message/message.component';
import { PageEvent } from '@angular/material/paginator';
import { FormGroup } from '@angular/forms';
import { RebelList } from './rebel-list';

@Component({
  selector: 'app-rebel',
  templateUrl: './rebel.component.html',
  styleUrls: ['./rebel.component.sass']
})
export class RebelComponent implements OnInit {

  rebelForm: FormGroup;
  dataSource: RebelList[] = [];

  constructor(
    private rebelService: RebelService,
    private message: MessageComponent
    ) { }

  ngOnInit(): void {
    this.getList();
  }

  getList() {
    this.rebelService.getAllRebel().subscribe((result) => this.dataSource = result);
  }

}
