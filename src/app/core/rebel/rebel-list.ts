export interface RebelList {
    id: number;
    name: string;
    token?: string;
    age: number;
    score?: number;
    traitor?: boolean;
    genre: string;
}