import { Component, OnInit } from '@angular/core';
import { RebelService } from '../rebel.service';
import { RebelDetail } from './dto/rebel-detail';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ItemDetail } from './dto/item-detail';
import { Router } from '@angular/router';

@Component({
  selector: 'app-property-detail',
  templateUrl: './rebel-detail.component.html',
  styleUrls: ['./rebel-detail.component.sass']
})
export class RebelDetailComponent implements OnInit {

  rebelDetailForm: FormGroup;
  rebel: RebelDetail = null;
  itens: ItemDetail[];
  rebelsInvolvedDto: any = {
    rebelSolicitor: String = null,
    rebelRequested: String = null
  };

  constructor(
    private service: RebelService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.getRebelDetail();
  }

  private getRebelDetail() {
    this.service.getRebelToken(this.route.snapshot.paramMap.get("token"))
      .subscribe((result) => {
        this.rebel = result,
        this.rebelsInvolvedDto.rebelSolicitor = result.token;
      });
  }

  submit(){
    this.service.negotiate(this.rebelsInvolvedDto)
      .subscribe(() => { 
        this.router.navigate(['rebel']);
      });
  }
}