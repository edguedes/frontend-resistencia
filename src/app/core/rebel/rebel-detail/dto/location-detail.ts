export interface LocationDetail {
    id: number;
    base: string;
    latitude: number;
    longitude: number;
}