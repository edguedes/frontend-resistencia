export interface ItemDetail {
    id: number;
    resource: string;
    valueItem?: number;
}