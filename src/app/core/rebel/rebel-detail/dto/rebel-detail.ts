import { LocationDetail } from './location-detail';
import { ItemDetail } from './item-detail';

export interface RebelDetail {
    id: number;
    name: string;
    token?: string;
    age: number;
    score?: number;
    traitor?: boolean;
    genre: string;
    location: LocationDetail;
    itens: ItemDetail[];
    
}
