import { Component, OnInit } from '@angular/core';
import { RebelService } from '../rebel.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AddressService } from '../../utils/consult-cep/address.service';
import { MessageComponent } from '../../utils/message/message.component';
import { CreateRebelDTO } from '../dto/create-rebel-dto';
import { ItemArray } from '../dto/itemArray';

@Component({
  selector: 'app-property-form',
  templateUrl: './rebel-form.component.html',
  styleUrls: ['./rebel-form.component.sass']
})
export class RebelFormComponent implements OnInit {

  rebelForm: FormGroup;
  rebelDto: CreateRebelDTO;
  itensArray: ItemArray[] = [];

  constructor(
    private service: RebelService,
    private formBuilder: FormBuilder,
    private message: MessageComponent,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.createForm(new CreateRebelDTO);
  }

  createForm(rebelDto: CreateRebelDTO){
    this.rebelForm = this.formBuilder.group({
      name: [rebelDto.name, [Validators.required,
        Validators.minLength(3),
        Validators.maxLength(40)]],
        age: [rebelDto.age, [
        Validators.required,
        Validators.pattern(/^[\d\s]+$/)
      ]],
      genre: [rebelDto.genre],
      locationDTO: this.formBuilder.group({
        base: [rebelDto.locationDTO.base, [Validators.required,
          Validators.minLength(3),
          Validators.maxLength(40)]],
        latitude: [rebelDto.locationDTO.latitude, [Validators.required,
          Validators.minLength(2),
          Validators.pattern(/^[\d\s]+$/)]],
        longitude: [rebelDto.locationDTO.longitude, [Validators.required,
            Validators.minLength(2),
            Validators.pattern(/^[\d\s]+$/)]],  
      }),
      itensDTO: []
    });
  }

  onChangeItem(event, item: string){ // Use appropriate model type instead of any
    this.itensArray.push(new ItemArray(item));
    }
  

  submit() {
    if (this.rebelForm.valid) {
      this.rebelForm.controls['itensDTO'].setValue(this.itensArray);
      console.log(this.rebelForm.value);
      this.service.createRebel(this.rebelForm.value).subscribe(
        () => {
          this.router.navigate(['rebel']);
        },
        (err) => {
          console.log(err);
        }
      );
    }
  }


}
