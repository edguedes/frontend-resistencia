import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Address } from './address';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  readonly url = 'https://viacep.com.br/ws';

  constructor(private http: HttpClient) { }

  getCep(cep: string): Observable<Address> {
    return this.http.get<Address>(`${this.url}/${cep}/json/`);
  }
}
