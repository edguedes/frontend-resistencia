export interface Address {
    bairro: string;
    cep: string;
    logradouro: string;
    complemento: string;
    localidade: string;
    uf: string;
    unidade: string;
    ibge: string;
    gia: string;
    erro: boolean;
}
