import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.sass']
})
export class MessageComponent implements OnInit {

  constructor(    
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    
  }

  sendSnackBar(status: number, message: string) {
    let snack = new MatSnackBarConfig();
    snack.duration = 5000;

    if (status === 201 || status === 200) {
      snack.panelClass = ['snack_ok'];
    } else if (status === 0) {
      snack.panelClass = ['snack_error'];
      message = 'Falha de conexão com o servidor';
    } else if (status === 500) {
      snack.panelClass = ['snack_error'];
      message = 'Erro do servidor';
    } else {
      snack.panelClass = ['snack_error'];
    }

    if (message !== "") {
      this.snackbar.open(message, '', snack);
    }
  }

}
